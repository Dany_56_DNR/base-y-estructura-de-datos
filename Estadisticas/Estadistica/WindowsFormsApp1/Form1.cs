﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1


{
    public partial class Form1 : Form



    {
        SqlConnection Conexion = new SqlConnection("Server=DESKTOP-D6NHSKT;DataBase=Practica_Dashboard;Integrated Security=true");
        SqlCommand cmd;
        SqlDataReader dr;

        public Form1()
        {
            InitializeComponent();
        }

        ArrayList Categoria = new ArrayList();
        ArrayList CantProd = new ArrayList();
        private void GrafCategorias()
        {
            cmd = new SqlCommand("ProdPorCategoria", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            Conexion.Open();
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                Categoria.Add(dr.GetString(0));
                CantProd.Add(dr.GetInt32(1));
            }
            chartProdxCategoria.Series[0].Points.DataBindXY(Categoria, CantProd);
            dr.Close();
            Conexion.Close();
        }



        ArrayList Producto = new ArrayList();
        ArrayList Cant = new ArrayList();
        private void ProdPreferidos()
        {
            cmd = new SqlCommand("ProdPreferidos", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            Conexion.Open();
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                Producto.Add(dr.GetString(0));
                Cant.Add(dr.GetInt32(1));
            }
            chartProdPreferidos.Series[0].Points.DataBindXY(Producto, Cant);
            dr.Close();
            Conexion.Close();
        }
        private void DashboardDatos()
        {
            cmd = new SqlCommand("DashboardDatos", Conexion);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlParameter total = new SqlParameter("@totVentas", 0); total.Direction = ParameterDirection.Output;
            SqlParameter nprod = new SqlParameter("@nprod", 0); nprod.Direction = ParameterDirection.Output;
            SqlParameter nmarca = new SqlParameter("@nmarc", 0); nmarca.Direction = ParameterDirection.Output;
            SqlParameter ncategora = new SqlParameter("@ncateg", 0); ncategora.Direction = ParameterDirection.Output;
            SqlParameter ncliente = new SqlParameter("@nclient", 0); ncliente.Direction = ParameterDirection.Output;
            SqlParameter nproveedores = new SqlParameter("@nprove", 0); nproveedores.Direction = ParameterDirection.Output;
            SqlParameter nempleados = new SqlParameter("@nemple", 0); nempleados.Direction = ParameterDirection.Output;

            cmd.Parameters.Add(total);
            cmd.Parameters.Add(nprod);
            cmd.Parameters.Add(nmarca);
            cmd.Parameters.Add(ncategora);
            cmd.Parameters.Add(ncliente);
            cmd.Parameters.Add(nproveedores);
            cmd.Parameters.Add(nempleados);

            Conexion.Open();
            cmd.ExecuteNonQuery();

            lblTotalVentas.Text = cmd.Parameters["@totVentas"].Value.ToString();
            lblCantCateg.Text = cmd.Parameters["@ncateg"].Value.ToString();
            lblCAntMarcas.Text = cmd.Parameters["@nmarc"].Value.ToString();
            lblCantProd.Text = cmd.Parameters["@nprod"].Value.ToString();
            lblCantClient.Text = cmd.Parameters["@nclient"].Value.ToString();
            lblCantEmple.Text = cmd.Parameters["@nemple"].Value.ToString();
            lblCantProve.Text = cmd.Parameters["@nprove"].Value.ToString();

            Conexion.Close();
        }


        private void chartProdxCategoria_Click(object sender, EventArgs e)
        {
          
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            GrafCategorias();
            ProdPreferidos();
            DashboardDatos();

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }
    }

}
