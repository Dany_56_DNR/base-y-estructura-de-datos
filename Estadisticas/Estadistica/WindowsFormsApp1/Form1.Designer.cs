﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.chartProdxCategoria = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.lblTotalVentas = new System.Windows.Forms.Label();
            this.lblCantCateg = new System.Windows.Forms.Label();
            this.lblCAntMarcas = new System.Windows.Forms.Label();
            this.lblCantProd = new System.Windows.Forms.Label();
            this.lblCantClient = new System.Windows.Forms.Label();
            this.lblCantEmple = new System.Windows.Forms.Label();
            this.lblCantProve = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.chartProdPreferidos = new System.Windows.Forms.DataVisualization.Charting.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.chartProdxCategoria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartProdPreferidos)).BeginInit();
            this.SuspendLayout();
            // 
            // chartProdxCategoria
            // 
            chartArea1.Name = "ChartArea1";
            this.chartProdxCategoria.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chartProdxCategoria.Legends.Add(legend1);
            this.chartProdxCategoria.Location = new System.Drawing.Point(15, 176);
            this.chartProdxCategoria.Name = "chartProdxCategoria";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chartProdxCategoria.Series.Add(series1);
            this.chartProdxCategoria.Size = new System.Drawing.Size(662, 353);
            this.chartProdxCategoria.TabIndex = 0;
            this.chartProdxCategoria.Text = "Categoria";
            this.chartProdxCategoria.Click += new System.EventHandler(this.chartProdxCategoria_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(15, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(114, 67);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(135, 12);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(168, 70);
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Location = new System.Drawing.Point(322, 12);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(146, 70);
            this.pictureBox3.TabIndex = 4;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Location = new System.Drawing.Point(483, 12);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(139, 70);
            this.pictureBox4.TabIndex = 5;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Location = new System.Drawing.Point(628, 12);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(160, 70);
            this.pictureBox5.TabIndex = 6;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Location = new System.Drawing.Point(794, 14);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(175, 68);
            this.pictureBox6.TabIndex = 7;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Location = new System.Drawing.Point(994, 9);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(164, 70);
            this.pictureBox7.TabIndex = 8;
            this.pictureBox7.TabStop = false;
            // 
            // lblTotalVentas
            // 
            this.lblTotalVentas.AutoSize = true;
            this.lblTotalVentas.Location = new System.Drawing.Point(26, 49);
            this.lblTotalVentas.Name = "lblTotalVentas";
            this.lblTotalVentas.Size = new System.Drawing.Size(15, 16);
            this.lblTotalVentas.TabIndex = 10;
            this.lblTotalVentas.Text = "F";
            // 
            // lblCantCateg
            // 
            this.lblCantCateg.AutoSize = true;
            this.lblCantCateg.Location = new System.Drawing.Point(151, 49);
            this.lblCantCateg.Name = "lblCantCateg";
            this.lblCantCateg.Size = new System.Drawing.Size(10, 16);
            this.lblCantCateg.TabIndex = 11;
            this.lblCantCateg.Text = "f";
            // 
            // lblCAntMarcas
            // 
            this.lblCAntMarcas.AutoSize = true;
            this.lblCAntMarcas.Location = new System.Drawing.Point(332, 49);
            this.lblCAntMarcas.Name = "lblCAntMarcas";
            this.lblCAntMarcas.Size = new System.Drawing.Size(44, 16);
            this.lblCAntMarcas.TabIndex = 12;
            this.lblCAntMarcas.Text = "label3";
            // 
            // lblCantProd
            // 
            this.lblCantProd.AutoSize = true;
            this.lblCantProd.Location = new System.Drawing.Point(492, 49);
            this.lblCantProd.Name = "lblCantProd";
            this.lblCantProd.Size = new System.Drawing.Size(44, 16);
            this.lblCantProd.TabIndex = 13;
            this.lblCantProd.Text = "label4";
            // 
            // lblCantClient
            // 
            this.lblCantClient.AutoSize = true;
            this.lblCantClient.Location = new System.Drawing.Point(633, 49);
            this.lblCantClient.Name = "lblCantClient";
            this.lblCantClient.Size = new System.Drawing.Size(44, 16);
            this.lblCantClient.TabIndex = 14;
            this.lblCantClient.Text = "label5";
            // 
            // lblCantEmple
            // 
            this.lblCantEmple.AutoSize = true;
            this.lblCantEmple.Location = new System.Drawing.Point(804, 49);
            this.lblCantEmple.Name = "lblCantEmple";
            this.lblCantEmple.Size = new System.Drawing.Size(44, 16);
            this.lblCantEmple.TabIndex = 15;
            this.lblCantEmple.Text = "label6";
            // 
            // lblCantProve
            // 
            this.lblCantProve.AutoSize = true;
            this.lblCantProve.Location = new System.Drawing.Point(1007, 49);
            this.lblCantProve.Name = "lblCantProve";
            this.lblCantProve.Size = new System.Drawing.Size(44, 16);
            this.lblCantProve.TabIndex = 16;
            this.lblCantProve.Text = "label7";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 16);
            this.label1.TabIndex = 17;
            this.label1.Text = "Total Ventas";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(151, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 16);
            this.label2.TabIndex = 18;
            this.label2.Text = "Cantidad Productos";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(332, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(133, 16);
            this.label3.TabIndex = 19;
            this.label3.Text = "Cantidad  Categorias";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(494, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 16);
            this.label4.TabIndex = 20;
            this.label4.Text = "Cantidad Marcas";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(643, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(112, 16);
            this.label5.TabIndex = 21;
            this.label5.Text = "Cantidad Clientes";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(804, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(130, 16);
            this.label6.TabIndex = 22;
            this.label6.Text = "Cantidad Empleado ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(1007, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(146, 16);
            this.label7.TabIndex = 23;
            this.label7.Text = "Cantidad Proevedores ";
            // 
            // chartProdPreferidos
            // 
            chartArea2.Name = "ChartArea1";
            this.chartProdPreferidos.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chartProdPreferidos.Legends.Add(legend2);
            this.chartProdPreferidos.Location = new System.Drawing.Point(701, 176);
            this.chartProdPreferidos.Name = "chartProdPreferidos";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Doughnut;
            series2.IsValueShownAsLabel = true;
            series2.IsXValueIndexed = true;
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.chartProdPreferidos.Series.Add(series2);
            this.chartProdPreferidos.Size = new System.Drawing.Size(517, 352);
            this.chartProdPreferidos.TabIndex = 24;
            this.chartProdPreferidos.Text = "chart1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1246, 600);
            this.Controls.Add(this.chartProdPreferidos);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblCantProve);
            this.Controls.Add(this.lblCantEmple);
            this.Controls.Add(this.lblCantClient);
            this.Controls.Add(this.lblCantProd);
            this.Controls.Add(this.lblCAntMarcas);
            this.Controls.Add(this.lblCantCateg);
            this.Controls.Add(this.lblTotalVentas);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.chartProdxCategoria);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chartProdxCategoria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartProdPreferidos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chartProdxCategoria;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Label lblTotalVentas;
        private System.Windows.Forms.Label lblCantCateg;
        private System.Windows.Forms.Label lblCAntMarcas;
        private System.Windows.Forms.Label lblCantProd;
        private System.Windows.Forms.Label lblCantClient;
        private System.Windows.Forms.Label lblCantEmple;
        private System.Windows.Forms.Label lblCantProve;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartProdPreferidos;
    }
}

