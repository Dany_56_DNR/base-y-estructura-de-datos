﻿Create database Clinica_BS

use Clinica_BS

CREATE TABLE DEPT (
DEPT_NO int identity (1,1) primary key,
DNOMBRE VARCHAR(50), 
LOC VARCHAR(50)
);

CREATE TABLE EMP (
ID_EMP int identity (1,1) primary key,
APELLIDO VARCHAR(50),  
EDAD INT CHECK(EDAD >17 ),
OFICIO VARCHAR(50),
DIR INT,
FECHA_ALT DATE,
SALARIO INT,
COMISION INT,
DEPT_NO INT references DEPT
);

CREATE TABLE ENFERMO(
INSCRIPCION INT,
APELLIDO VARCHAR(50), 
DIRECCION VARCHAR(50), 
FECHA_NAC DATE,
S VARCHAR (50),
NSS INT
);

-- Registros para DEPT
insert into DEPT(DNOMBRE,LOC) values('Cardiologia','Lima');
insert into DEPT(DNOMBRE,LOC) values('Pediatria','Cusco');
insert into DEPT(DNOMBRE,LOC) values('Dermatologia','Arequipa');

-- Registros para ENFERMO
insert into ENFERMO (APELLIDO,DIRECCION,FECHA_NAC,S,NSS) values('Rodriguez','Jr. Ayacucho 123', '1990-03-10', 'Masculino', '123456789');
insert into ENFERMO (APELLIDO,DIRECCION,FECHA_NAC,S,NSS) values('Lopez','Av. Tacna 456', '2005-08-20', 'Femenino', '987654321');
insert into ENFERMO (APELLIDO,DIRECCION,FECHA_NAC,S,NSS) values('Martinez','Calle Junin 789', '1985-12-05', 'Masculino', '555555555');

-- Registros para EMP
insert into EMP (APELLIDO,EDAD, OFICIO, DIR,FECHA_ALT,SALARIO,COMISION,DEPT_NO) values ('Gonzalez',25,'Medico','789','2020-05-15','45000','1500', '1');
insert into EMP (APELLIDO,EDAD, OFICIO, DIR,FECHA_ALT,SALARIO,COMISION,DEPT_NO) values ('Perez',32,'Enfermera','321','2018-08-03','35000','800', '2');
insert into EMP (APELLIDO,EDAD, OFICIO, DIR,FECHA_ALT,SALARIO,COMISION,DEPT_NO) values ('Fernandez',40,'Administrador','456','2017-10-21','55000','2000', '3');



-- 1. Crear procedimientos almacenados y funciones

-- Procedimiento para obtener empleados que se dieron de alta antes de 2018 en un departamento específico.
CREATE PROCEDURE Empleados_Antes_2018 ( @DeptNombre VARCHAR(50))
AS BEGIN
SELECT EMP.* FROM EMP INNER JOIN DEPT ON EMP.DEPT_NO = DEPT.DEPT_NO
WHERE DEPT.DNOMBRE = @DeptNombre AND YEAR(EMP.FECHA_ALT) < 2018;
END

-- Procedimiento para insertar un nuevo departamento.
CREATE PROCEDURE InsertarDepartamento (
    @NombreDepartamento VARCHAR(50),
    @Ubicacion VARCHAR(50)
)
AS
BEGIN
    INSERT INTO DEPT (DNOMBRE, LOC) VALUES (@NombreDepartamento, @Ubicacion);
END

-- Procedimiento para recuperar el promedio de edad de las personas por departamento.
CREATE PROCEDURE Promedio_Edad_Departamento
AS
BEGIN
    SELECT DEPT.DNOMBRE, AVG(EMP.EDAD) AS PromedioEdad
    FROM DEPT
    LEFT JOIN EMP ON DEPT.DEPT_NO = EMP.DEPT_NO
    GROUP BY DEPT.DNOMBRE;
END

-- Procedimiento para obtener apellido, oficio y salario por numero de empleado.
CREATE PROCEDURE Datos_Empleado (
    @NumeroEmpleado INT
)
AS
BEGIN
    SELECT APELLIDO, OFICIO, SALARIO
    FROM EMP
    WHERE ID_EMP = @NumeroEmpleado;
END

-- Procedimiento para dar de baja a un empleado por apellido.
CREATE PROCEDURE Dar_Baja_Empleado_Apellido (
    @ApellidoEmpleado VARCHAR(50)
)
AS
BEGIN
    DELETE FROM EMP
    WHERE APELLIDO = @ApellidoEmpleado;
END

-- 2. Crear restricciones en la base de datos

-- Restricion UNIQUE en el campo NSS de la tabla ENFERMO.
ALTER TABLE ENFERMO
ADD CONSTRAINT UQ_NSS UNIQUE (NSS);

-- Restriccion DEFAULT en el campo COMISION de la tabla EMP.
ALTER TABLE EMP
ADD CONSTRAINT DF_COMISION DEFAULT 0 FOR COMISION;
