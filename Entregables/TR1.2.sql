
-- 3. Desarrolla programas de Transact SQL para la creaci�n de una tienda virtual

--tabla para productos
CREATE TABLE PRODUCTOS (
    ID_PRODUCTO INT IDENTITY(1,1) PRIMARY KEY,
    NOMBRE VARCHAR(100),
    PRECIO DECIMAL(10, 2),
    STOCK INT
);

-- Procedimiento almacenado para agregar un nuevo producto
CREATE PROCEDURE AgregarProducto
(
    @NombreProducto VARCHAR(100),
    @Precio DECIMAL(10, 2),
    @Stock INT
)
AS
BEGIN
    INSERT INTO PRODUCTOS (NOMBRE, PRECIO, STOCK)
    VALUES (@NombreProducto, @Precio, @Stock);
END

-- Procedimiento almacenado para consultar productos
CREATE PROCEDURE ConsultarProductos
AS
BEGIN
    SELECT * FROM PRODUCTOS;
END
---Procedimiento almacenado Para Realizar Compra 
CREATE PROCEDURE RealizarCompra
(
    @ClienteID INT,
    @ProductoID INT,
    @Cantidad INT
)
AS
BEGIN
    DECLARE @PrecioProducto DECIMAL(10, 2);

    -- Obtener el precio del producto
    SELECT @PrecioProducto = PRECIO FROM PRODUCTOS WHERE ID_PRODUCTO = @ProductoID;

    -- Insertar la compra en la tabla de compras
    INSERT INTO COMPRAS (CLIENTE_ID, PRODUCTO_ID, CANTIDAD, PRECIO_UNITARIO)
    VALUES (@ClienteID, @ProductoID, @Cantidad, @PrecioProducto);
END

-- Procedimiento almacenado para modificar un producto
CREATE PROCEDURE ModificarProducto
(
    @ID_Producto INT,
    @NuevoPrecio DECIMAL(10, 2),
    @NuevoStock INT
)
AS
BEGIN
    UPDATE PRODUCTOS
    SET PRECIO = @NuevoPrecio, STOCK = @NuevoStock
    WHERE ID_PRODUCTO = @ID_Producto;
END

-- Procedimiento almacenado para eliminar un producto
CREATE PROCEDURE EliminarProducto
(
    @ID_Producto INT
)
AS
BEGIN
    DELETE FROM PRODUCTOS
    WHERE ID_PRODUCTO = @ID_Producto;
END

-- 4. Realiza consultas avanzadas en BD para la generacion de informes estadisticos

-- Consulta 1: Listar productos mas vendidos
CREATE PROCEDURE ProductosMasVendidos
AS
BEGIN
    SELECT TOP 10 NOMBRE AS Producto, SUM(CANTIDAD) AS TotalVentas
    FROM PRODUCTOS
    INNER JOIN COMPRAS ON PRODUCTOS.ID_PRODUCTO = COMPRAS.ID_PRODUCTO
    GROUP BY NOMBRE
    ORDER BY TotalVentas DESC;
END

-- Consulta 2: Calcular ventas totales
CREATE FUNCTION VentasTotales
(
    @FechaInicio DATE,
    @FechaFin DATE
)
RETURNS DECIMAL(10, 2)
AS
BEGIN
    DECLARE @TotalVentas DECIMAL(10, 2);
    SELECT @TotalVentas = SUM(PRECIO * CANTIDAD)
    FROM COMPRAS
    WHERE FECHA_COMPRA BETWEEN @FechaInicio AND @FechaFin;
    RETURN @TotalVentas;
END
-- Consulta 3: Listar clientes con mayor gasto
CREATE PROCEDURE ClientesConMayorGasto
AS
BEGIN
    SELECT C.NOMBRE AS Cliente, SUM(P.PRECIO * CO.CANTIDAD) AS GastoTotal
    FROM CLIENTES C
    INNER JOIN COMPRAS CO ON C.ID_CLIENTE = CO.CLIENTE_ID
    INNER JOIN PRODUCTOS P ON CO.PRODUCTO_ID = P.ID_PRODUCTO
    GROUP BY C.NOMBRE
    ORDER BY GastoTotal DESC;
END


-- Consulta 4: Ventas mensuales por departamento
CREATE PROCEDURE VentasMensualesPorDepartamento
AS
BEGIN
    SELECT
        YEAR(C.FECHA_COMPRA) AS A�o,
        MONTH(C.FECHA_COMPRA) AS Mes,
        D.DNOMBRE AS Departamento,
        SUM(P.PRECIO * CO.CANTIDAD) AS VentasMensuales
    FROM DEPT D
    INNER JOIN EMP E ON D.DEPT_NO = E.DEPT_NO
    INNER JOIN COMPRAS CO ON E.ID_EMP = CO.EMPLEADO_ID
    INNER JOIN PRODUCTOS P ON CO.PRODUCTO_ID = P.ID_PRODUCTO
    GROUP BY YEAR(C.FECHA_COMPRA), MONTH(C.FECHA_COMPRA), D.DNOMBRE
    ORDER BY A�o, Mes;
END

-- Consulta 5: Ventas diarias en un periodo especifico
CREATE PROCEDURE VentasDiariasEnPeriodo
(
    @FechaInicio DATE,
    @FechaFin DATE
)
AS
BEGIN
    SELECT
        FECHA_COMPRA AS Fecha,
        SUM(PRECIO * CANTIDAD) AS VentasDiarias
    FROM COMPRAS
    WHERE FECHA_COMPRA BETWEEN @FechaInicio AND @FechaFin
    GROUP BY FECHA_COMPRA
    ORDER BY Fecha;
END
