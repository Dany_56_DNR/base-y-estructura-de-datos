

-- Danner Antonio Zurita Perez 

use biblioteca;

CREATE TABLE clase (
    Clave CHAR(10) PRIMARY KEY,
    Tiempo_de_prestamo INT
);

CREATE TABLE libro (
    Codigo VARCHAR(10) PRIMARY KEY,
    Autor VARCHAR(50),
    Titulo VARCHAR(50),
    Editor VARCHAR(30),
    Prestado INT DEFAULT 1,
    Clase CHAR(10) REFERENCES clase(Clave) 
);

CREATE TABLE usuario (
    Secuencia VARCHAR(10) PRIMARY KEY,
    Nombre VARCHAR(50),
    Direccion VARCHAR(50),
    Fecha_ingreso DATETIME NOT NULL,
    Sexo CHAR(1)
);

CREATE TABLE prestamo (
    Codigo VARCHAR(10),
    Secuencia VARCHAR(10),
    Fecha_inicio DATETIME,
    PRIMARY KEY (Codigo, Secuencia),
    FOREIGN KEY (Codigo) REFERENCES libro(Codigo),
    FOREIGN KEY (Secuencia) REFERENCES usuario(Secuencia)
);


--1 eje 
ALTER TABLE usuario ADD sexo char;

ALTER TABLE libro ADD indice int;

--2 insercion de datos 

INSERT INTO clase VALUES(1, 15);
INSERT INTO clase VALUES(2, 30);
INSERT INTO clase VALUES(3, 365);

INSERT INTO libro VALUES ('INF01', 'Pepe', 'Los problemas', 'MG', 1, 1, 1);
INSERT INTO libro VALUES ('INF02', 'Paco', 'Psicolog�a', 'LL', 1, 1, 1);
INSERT INTO libro VALUES ('FIS01', 'Pepe', 'Fundamentos f�sicos', 'RO-MO', 2, 1, 1);
INSERT INTO libro VALUES ('MAT02', 'Lola', 'C�lculo', 'MM', 3, 1, 1);
INSERT INTO libro VALUES ('MAT05', 'Susi', 'Matem�ticas discretas', 'MM', 2, 1, 1);

INSERT INTO usuario VALUES(1, 'Luz', 'Alarcos', '1989-01-01', 'F');
INSERT INTO usuario VALUES(2, 'Isa', 'Alarcos', '1999-08-07', 'F');
INSERT INTO usuario VALUES(3, 'Danner', 'Postas', '2001-02-06', 'M');
INSERT INTO usuario VALUES(4, 'Fiorela', 'Calatrava', '2001-04-03', 'F');
INSERT INTO usuario VALUES(5, 'Percy', 'Encinas', '1986-05-02', 'M');

INSERT INTO prestamo VALUES('INF01', 1, '2005-06-01');
INSERT INTO prestamo VALUES('INF02', 1, '2005-06-01');
INSERT INTO prestamo VALUES('FIS01', 2, '2005-05-07');
INSERT INTO prestamo VALUES('MAT02', 3, '2005-08-02');
INSERT INTO prestamo VALUES('MAT05', 3, '2005-06-02');


--3 modificsr clases
UPDATE clase SET tiempo_de_prestamo = 180 WHERE clave = 3;


--4 borrar 

DELETE FROM Usuario WHERE secuencia > 3;

SELECT Titulo FROM libro WHERE Codigo LIKE 'INF%' ORDER BY Titulo;


-- 5  selleccionar fecha

SELECT prestamo.fecha_inicio
FROM prestamo
INNER JOIN usuario ON Prestamo.Secuencia = usuario.Secuencia
WHERE usuario.Direccion = 'Alarcos';

--6

SELECT Fecha_inicio
FROM prestamo, libro, usuario
WHERE Prestamo.Secuencia = usuario.Secuencia
AND Prestamo.Codigo = libro.Codigo
AND editor = 'MM'
AND fecha_ingreso > '2000-12-31';


--7
SELECT Editor, count(libro.Codigo)
FROM libro, Prestamo
WHERE Prestamo.Codigo=libro.Codigo
GROUP BY Editor;

--8
SELECT Nombre
FROM usuario
WHERE Secuencia < ANY (
    SELECT Clase * 10
    FROM libro, Prestamo
    WHERE prestamo.codigo = libro.codigo
    AND Prestamo.Secuencia = usuario.Secuencia
);


--9

SELECT Fecha_ingreso
FROM usuario u
WHERE u.Secuencia > ALL (
    SELECT p.Codigo
    FROM Prestamo p
    WHERE p.Secuencia = u.Secuencia
);


--10

CREATE VIEW libro_x_persona_vw AS
SELECT l.Codigo, l.Titulo, l.Autor, u.Secuencia, u.Nombre, u.Sexo
FROM libro l
JOIN prestamo p ON l.Codigo = p.Codigo
JOIN usuario u ON p.Secuencia = u.Secuencia;


SELECT * FROM libro_x_persona_vw;
