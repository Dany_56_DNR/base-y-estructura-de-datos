
--EJERCICIO 2
---------------------------Danner Antonio Zuirta Perez -----------------

---1 Encuentre a todos los miembros del personal cuyo nombre empiece por 'H'.
SELECT APELLIDO
FROM PLANTILLA
WHERE UPPER(APELLIDO) LIKE 'K%';


---2. Quienes son las enfermeras y enfermeros que trabajan en turnos de Tarde o Ma�ana.
select apellido
from plantilla
where upper(funcion) in ('ENFERMERO' ,'ENFERMERA')
and upper(turno) in ('T','M');


--3. Haga un listado de las enfermeras que ganan entre 2.000.000 y 2.500.000 Pts.
select apellido, salario
from plantilla
where salario between 20 and 2500000
and upper(funcion)= 'ENFERMERA';

--4 Mostrar, para todos los hospitales, el c�digo de hospital, el nombre completo del hospital
--y su nombre abreviado de tres letras (a esto podemos llamarlo ABR) Ordenar la recuperaci�n por
--esta abreviatura.
SELECT
    HOSPITAL_COD AS "C�digo de Hospital",
    NOMBRE AS "Nombre Completo",
    SUBSTRING(NOMBRE, 1, 3) AS "ABR"
FROM HOSPITAL
ORDER BY "ABR";


-- En la tabla DOCTOR otorgar a Cardiolog�a un c�digo de 1, a Psiquiatr�a un c�digo de 2,
--a Pediatr�a un c�digo de 3 y a cualquier otra especialidad un c�digo de 4. Recuperar todos los
--doctores, su especialidad y el c�digo asignado.


SELECT APELLIDO, ESPECIALIDAD,
	CASE
        WHEN ESPECIALIDAD = 'Cardiolog�a' THEN '1'
        WHEN ESPECIALIDAD = 'Psiquiatr�a' THEN '2'
        WHEN ESPECIALIDAD = 'Pediatr�a' THEN '3'
        ELSE '4'
    END AS C�digo
FROM DOCTOR;


---6 Hacer un listado de los nombres de los pacientes y la posici�n de la primera letra 'A' que
--aparezca en su apellido, tomando como referencia la primera letra del mismo

SELECT
    APELLIDO,
    CHARINDEX('A', APELLIDO) AS "PRIMERA LETRA A"
FROM ENFERMO;


--7. Queremos conseguir:
SELECT
    'El departamento de ' + DNOMBRE + ' est� en ' + LOC AS COMENTARIO
FROM DEPT2;


--8. Para cada empleado cuyo apellido contenga una "N", queremos que nos devuelva "nnn",
--pero solo para la primera ocurrencia de la "N". La salida debe estar ordenada por apellido
--ascendentemente.

SELECT
    STUFF(apellido, CHARINDEX('N', apellido, 1), 1, 'nnn') AS "TRES N"
FROM emp
WHERE CHARINDEX('N', apellido, 1) > 0
ORDER BY apellido;


--9. Para cada empleado se pide que salga su salario total (salario mas comisi�n) y luego su
--salario fra--gmentado, es decir, en centenas de mil, decenas de mil... decenas y unidades. La salida
--debe estar -- ordenada por el salario y el apellido descend�ntemente.

SELECT
    apellido,
    salario + ISNULL(comision, 0) AS sal_total,
    SUBSTRING(CONVERT(VARCHAR, salario + ISNULL(comision, 0)), 1, 1) AS c,
    SUBSTRING(CONVERT(VARCHAR, salario + ISNULL(comision, 0)), 2, 1) AS d,
    SUBSTRING(CONVERT(VARCHAR, salario + ISNULL(comision, 0)), 3, 1) AS m,
    SUBSTRING(CONVERT(VARCHAR, salario + ISNULL(comision, 0)), 4, 1) AS c2,
    SUBSTRING(CONVERT(VARCHAR, salario + ISNULL(comision, 0)), 5, 1) AS d2,
    SUBSTRING(CONVERT(VARCHAR, salario + ISNULL(comision, 0)), 6, 1) AS u
FROM emp
ORDER BY sal_total DESC, apellido;


-- 10. Para cada empleado que no tenga comisi�n o cuya comisi�n sea mayor que el 15% de su
--salario, se pide el salario total que tiene. Este ser�: si tiene comisi�n su salario mas su comisi�n,
--y si no tiene, su salario mas su nueva comisi�n (15% del salario). La salida deber� estar
--ordenada por el oficio y por el salario que le queda descend�ntemente.

SELECT
    apellido,
    oficio,
    salario + ISNULL(comision, salario * 0.15) AS salario_total
FROM emp
WHERE comision IS NULL OR comision > salario * 0.15
ORDER BY oficio, salario_total DESC;


--11. Encuentre a todas las enfermeras y enfermeros con indicaci�n del salario mensual de cada
--uno.

SELECT
    apellido,
    FLOOR(salario / 12) AS "SALARIO MENSUAL"
FROM plantilla
WHERE UPPER(funcion) IN ('ENFERMERA', 'ENFERMERO');


--12. Que fecha fue hace tres semanas?

SELECT DATEADD(DAY, -21, GETDATE()) AS fecha;


--13. Se pide el nombre, oficio y fecha de alta del personal del departamento 20 que ganan mas de
--150000 ptas. mensuales.

SELECT
    apellido,
    oficio,
    CONVERT(NVARCHAR, fecha_alta, 106) + ' de ' + CONVERT(NVARCHAR, YEAR(fecha_alta)) + ' ' + REPLACE(CONVERT(NVARCHAR, FORMAT(fecha_alta, 'HH:mm'), 108), ':', '.') AS alta
FROM emp
WHERE dept_no = 20 AND salario > 150000;






--14. Se pide el nombre, oficio y el d�a de la semana en que han sido dados de alta los
--empleados de la empresa, pero solo de aquellos cuyo d�a de alta haya sido entre martes y jueves.
--Ordenado por oficio.
SELECT
    emp_no,
    oficio,
    DATENAME(WEEKDAY, fecha_alta) AS d�a
FROM emp
WHERE DATENAME(WEEKDAY, fecha_alta) IN ('Tuesday', 'Wednesday', 'Thursday')
ORDER BY oficio;


--15. Para todos los empleados, el d�a en que fueron dados de alta en la empresa debe estar
--ordenada por el d�a de la semana (Lunes, Martes ... Viernes) . Los d�as no laborables ser�n "Fin
--de semana".

SELECT
    apellido,
    oficio,
    CASE
        WHEN DATENAME(WEEKDAY, fecha_alta) = 'Monday' THEN 'Lunes'
        WHEN DATENAME(WEEKDAY, fecha_alta) = 'Tuesday' THEN 'Martes'
        WHEN DATENAME(WEEKDAY, fecha_alta) = 'Wednesday' THEN 'Mi�rcoles'
        WHEN DATENAME(WEEKDAY, fecha_alta) = 'Thursday' THEN 'Jueves'
        WHEN DATENAME(WEEKDAY, fecha_alta) = 'Friday' THEN 'Viernes'
        ELSE 'Fin de semana'
    END AS d�a
FROM emp
ORDER BY 3;


--16. Encontrar el salario medio de los Analistas.

SELECT AVG(salario) AS "SALARIO MEDIO"
FROM emp
WHERE UPPER(oficio) = 'ANALISTA';


--17. Encontrar el salario mas alto y el salario mas bajo de la tabla de empleados, as� como la
--diferencia entre ambos.

SELECT
    MAX(salario) AS maximo,
    MIN(salario) AS m�nimo,
    MAX(salario) - MIN(salario) AS diferencia
FROM emp;


--18. Calcular el numero de oficios diferentes que hay, en total, en los departamentos 10 y 20
--de la empresa.

SELECT COUNT(DISTINCT oficio) AS tareas
FROM emp
WHERE dept_no IN (10, 20);


--19. Calcular el numero de personas que realizan cada oficio en cada departamento.

SELECT
    dept_no,
    oficio,
    COUNT(*) AS total_empleados
FROM emp
GROUP BY dept_no, oficio;

--20. Buscar que departamentos tienen mas de cuatro personas trabajando.

SELECT
    dept_no,
    COUNT(*) AS total_empleados
FROM emp
GROUP BY dept_no
HAVING COUNT(*) > 4;

--21. Buscar que departamentos tienen mas de dos personas trabajando en la misma profesi�n.

select dept_no, count(*)
from emp
group by dept_no, oficio
having count (*) > 2;

--22. Se desea saber el numero de empleados por departamento que tienen por oficio el de
--"EMPLEADO". La salida debe estar ordenada por el numero de departamento.

SELECT
    dept_no,
    COUNT(*) AS total_empleados
FROM emp
WHERE UPPER(oficio) = 'EMPLEADO'
GROUP BY dept_no;


--23. Se desea saber el salario total (salario mas comisi�n) medio anual de los vendedores de
--nuestra empresa.
SELECT
    oficio,
    AVG(salario + ISNULL(comision, salario * 0.15)) AS 'SALARIO MEDIO ANUAL'
FROM emp
WHERE UPPER(oficio) = 'VENDEDOR'
GROUP BY oficio;


--24. Se desea saber el salario total (salario mas comisi�n) medio anual, tanto de los empleados
--como de los vendedores de nuestra empresa.
SELECT
    oficio,
    AVG(salario + ISNULL(comision, salario * 0.15)) AS 'SALARIO MEDIO ANUAL'
FROM emp
WHERE UPPER(oficio) IN ('VENDEDOR', 'EMPLEADO')
GROUP BY oficio;

--25. Se desea saber para cada departamento y en cada oficio, el maximo salario y la suma total
--de salarios, pero solo de aquellos departamentos y oficios cuya suma salarial supere o sea igual
--que el 50% de su maximo salario. En el muestreo, solo se estudiaron a aquellos empleados que
--no tienen comisi�n o la tengan menor que el 25% de su salario.

SELECT
    dept_no,
    oficio,
    SUM(salario) AS suma,
    MAX(salario) AS maximo
FROM emp
WHERE (comision IS NULL) OR (comision < 0.25 * salario)
GROUP BY dept_no, oficio
HAVING SUM(salario) >= 0.5 * MAX(salario);



--26. Se desea saber para cada oficio, dentro de cada a�o de alta distinto que existe en nuestra 
--empresa, el numero de empleados y la media salarial que tiene. Para este estudio, no se tendr� en 
--cuenta a los empleados que no hayan sido dados de alta en un d�a laboral. Adem�s, solo se desea 
--saber estos datos, de aquellos oficios y a�os que tienen mas de un empleado. La salida debe 
--estar ordenada por el a�o de alta y la media salarial descend�ntemente
SELECT
    YEAR(fecha_alta) AS alta,
    oficio,
    COUNT(*) AS "N1 EMPL",
    AVG(salario * 1.0) AS "MEDIA SALARIAL"
FROM emp
WHERE DATEPART(WEEKDAY, fecha_alta) NOT IN (1, 7)
GROUP BY YEAR(fecha_alta), oficio
HAVING COUNT(*) > 1
ORDER BY alta, "MEDIA SALARIAL" DESC;

--27. Se desea saber, para cada inicial de apellido que exista en la empresa (tratando solo las 
--iniciales consonantes), el maximo salario que tiene asociada. No se tendr� en cuenta en el 
-- estudio a aquellos empleados que contengan en su apellido mas de una "N". La salida debe estar 
--ordenada por la inicial.

SELECT
    LEFT(apellido, 1) AS inicial,
    MAX(salario) AS maximo
FROM emp
WHERE CHARINDEX('N', apellido, CHARINDEX('N', apellido) + 1) = 0
    AND LEFT(apellido, 1) NOT IN ('A', 'E', 'I', 'O', 'U')
GROUP BY LEFT(apellido, 1)
ORDER BY inicial;




---28. Se desea obtener un informe matriz como el que se presenta, en el que la coordenada 
--vertical hace referencia a los distintos oficios existentes en la empresa, y la coordenada 
--horizontal a los distintos departamentos. Los valores de la matriz, indicaran la suma de salarios 
--por oficio y departamento. La ultima columna indica la suma total de salarios por oficio.SELECT
    oficio,
    SUM(CASE WHEN dept_no = 10 THEN salario ELSE 0 END) AS dep10,
    SUM(CASE WHEN dept_no = 20 THEN salario ELSE 0 END) AS dep20,
    SUM(CASE WHEN dept_no = 30 THEN salario ELSE 0 END) AS dep30,
    SUM(CASE WHEN dept_no = 40 THEN salario ELSE 0 END) AS dep40,
    SUM(salario) AS total
FROM emp
GROUP BY oficio
ORDER BY total DESC;
/*---29. Se desea saber para cada departamento y oficio, la suma total de comisiones, teniendo en 
--cuenta que para los empleados que no tienen comisi�n, se les asignara:
- El 10% de su salario si son del departamento 10.
- El 15% de su salario si son del departamento 20.
- El 17% de su salario si son del departamento 30.
- Cualquier otro departamento, el 5% de su salario.
No se tendr� en cuenta a los empleados que hayan sido dados de alta despu�s de 1981, ni al que 
ostente el cargo de "PRESIDENTE".*/SELECT
    dept_no,
    oficio,
    SUM(ISNULL(comision, CASE 
                WHEN dept_no = 10 THEN 0.1 * salario
                WHEN dept_no = 20 THEN 0.15 * salario
                WHEN dept_no = 30 THEN 0.17 * salario
                ELSE 0.05 * salario
            END
        )
    ) AS "SUMA DE COMISIONES"
FROM emp
WHERE YEAR(fecha_alta) <= 1981
    AND UPPER(oficio) != 'PRESIDENTE'
GROUP BY dept_no, oficio;
---30.- Queremos saber el m�ximo, el m�nimo y la media salarial, de cada departamento de la 
--empresa.SELECT 'Maximo---' AS comentario, MAX(salario) AS valor, dept_no
FROM emp
GROUP BY dept_no
UNION
SELECT 'Media--->' AS comentario, AVG(salario) AS valor, dept_no
FROM emp
GROUP BY dept_no
UNION
SELECT 'Minimo---' AS comentario, MIN(salario) AS valor, dept_no
FROM emp
GROUP BY dept_no
ORDER BY dept_no, valor;
--31. Listar, a partir de las tablas EMP y DEPT2, el nombre de cada empleado, su oficio, su 
--numero de departamento y el nombre del departamento donde trabajan.

select apellido, oficio, e.dept_no, dnombre
from emp e, dept2 d
where e.dept_no = d.dept_no;


--32. Seleccionar los nombres, profesiones y localidades de los departamentos donde trabajan 
--los Analistas.SELECT
    e.apellido AS nombre,
    e.oficio,
    d.loc
FROM emp e
JOIN dept2 d ON e.dept_no = d.dept_no
WHERE UPPER(e.oficio) = 'ANALISTA';
--33. Se desea conocer el nombre y oficio de todos aquellos empleados que trabajan en Madrid. 
--La salida deber� estar ordenada por el oficio.

SELECT
    e.apellido,
    e.oficio
FROM emp e
JOIN dept2 d ON e.dept_no = d.dept_no
WHERE UPPER(d.loc) = 'MADRID'
ORDER BY e.oficio;


---34. se desea conocer cuantos empleados existen en cada departamento. Devolviendo una 
--salida como la que se presenta (deber� estar ordenada por el numero de empleados 
--descend�ntemente).

SELECT
    e.dept_no AS num_dep,
    d.dnombre AS departamento,
    COUNT(*) AS n1_empl
FROM emp e
JOIN dept2 d ON e.dept_no = d.dept_no
GROUP BY e.dept_no, d.dnombre
ORDER BY n1_empl DESC;




/*
35. Se desea conocer, tanto para el departamento de VENTAS, como para el de 
CONTABILIDAD, su maximo, su m�nimo y su media salarial, as� como el numero de empleados 
en cada departamento. La salida deber� estar ordenada por el nombre del departamento, y se 
deber� presentar como la siguiente:*/SELECT
    d.dnombre,
    MAX(e.salario) AS maximo,
    MIN(e.salario) AS minimo,
    AVG(e.salario) AS media,
    COUNT(*) AS n_empl
FROM emp e
JOIN dept2 d ON e.dept_no = d.dept_no
WHERE UPPER(d.dnombre) IN ('VENTAS', 'CONTABILIDAD')
GROUP BY d.dnombre
ORDER BY d.dnombre;
---36. Se desea conocer el maximo salario que existe en cada sala de cada hospital, dando el 
--resultado como sigue:SELECT
    h.nombre AS hospital,
    s.nombre AS sala,
    MAX(p.salario) AS maximo
FROM sala s
JOIN plantilla p ON p.sala_cod = s.sala_cod
JOIN hospital h ON h.hospital_cod = p.hospital_cod
GROUP BY h.nombre, s.nombre;
--37. Se desea obtener un resultado como el que aparece, en el que se presenta el numero, 
--nombre y oficio de cada empleado de nuestra empresa que tiene jefe, y lo mismo de su jefe 
--directo. La salida debe estar ordenada por el nombre del empleado.

SELECT
    e.emp_no AS empleado,
    e.apellido AS nombre,
    e.oficio,
    e.dir AS jefe,
    e2.apellido AS nombre_jefe,
    e2.oficio AS oficio_jefe
FROM emp e
LEFT JOIN emp e2 ON e.dir = e2.emp_no
ORDER BY e.apellido;

---38. Se desea conocer, para todos los departamentos existentes, el m�nimo salario de cada 
--departamento, mostrando el resultado como aparece. Para el muestreo del m�nimo salario, no 
--queremos tener en cuenta a las personas con oficio de EMPLEADO. La salida estar� ordenada 
--por el salario descend�ntemente.
SELECT
    d.dnombre AS departamento,
    ISNULL(MIN(CASE WHEN e.oficio != 'EMPLEADO' THEN e.salario END), 0) AS minimo
FROM dept2 d
LEFT JOIN emp e ON e.dept_no = d.dept_no
GROUP BY d.dnombre
ORDER BY minimo DESC;


---39. Se desea sacar un listado con el mismo formato del ejercicio
--33 tal y como el que aparece, pero ahora tambi�n se desea sacar al jefe de la empresa como 
--empleado, pues en el listado del citado ejercicio no aparec�a.

SELECT
    e.emp_no AS empleado,
    e.apellido AS nombre,
    e.oficio,
    e.dir AS jefe,
    ISNULL(e2.apellido, 'PRESIDENTE') AS nombre_jefe,
    ISNULL(e2.oficio, 'PRESIDENTE') AS oficio_jefe
FROM emp e
LEFT JOIN emp e2 ON e.dir = e2.emp_no;


--40. Se desea obtener un listado como el que aparece, es decir, como el del ejercicio 33, pero 
--obteniendo adem�s todos aquellos empleados que no son jefes de nadie en la parte de jefe, para 
--que se vea gr�ficamente que no tienen subordinados a su cargo.
SELECT
    e.emp_no AS empleado,
    e.apellido AS nombre,
    e.oficio,
    ISNULL(e.dir, 0) AS jefe,
    ISNULL(e2.apellido, 'SIN JEFE') AS nombre_jefe,
    ISNULL(e2.oficio, 'SIN JEFE') AS oficio_jefe
FROM emp e
LEFT JOIN emp e2 ON e.dir = e2.emp_no
ORDER BY e.apellido;


--41. Se desea obtener un listado combinaci�n de los dos ejercicios anteriores.SELECT
    e.emp_no AS empleado,
    e.apellido AS nombre,
    e.oficio AS oficio,
    e.dir AS jefe,
    e2.apellido AS nombre_jefe,
    e2.oficio AS oficio_jefe
FROM emp e
LEFT JOIN emp e2 ON e.dir = e2.emp_no
UNION
SELECT
    e.emp_no AS empleado,
    e.apellido AS nombre,
    e.oficio AS oficio,
    e.dir AS jefe,
    e2.apellido AS nombre_jefe,
    e2.oficio AS oficio_jefe
FROM emp e
LEFT JOIN emp e2 ON e.dir = e2.emp_no
ORDER BY 2;
--42. Obtener el apellido, departamento y oficio de aquellos empleados que tengan un oficio 
--que este en el departamento 20 y que no sea ninguno de los oficios que esta en el departamento 
--de VENTAS.
SELECT
    apellido,
    dept_no,
    oficio
FROM emp
WHERE oficio IN (SELECT oficio FROM emp WHERE dept_no = 20)
AND oficio NOT IN (SELECT oficio FROM emp e, dept2 d WHERE e.dept_no = d.dept_no AND UPPER(dnombre) = 'VENTAS');


---43. Obtener el numero de empleado, numero de departamento y apellido de todos los 
--empleados que trabajen en el departamento 20 o 30 y su salario sea mayor que dos veces el 
--m�nimo de la empresa. No queremos que el oficio sea PRESIDENTE.
SELECT
    emp_no,
    dept_no,
    apellido
FROM emp
WHERE dept_no IN (20, 30)
AND salario > (SELECT 2 * MIN(salario) FROM emp)
AND UPPER(oficio) NOT LIKE 'PRES%';

--44. Encontrar las personas que ganan 500.000 PTA mas que el miembro del personal de 
--sueldo mas alto del turno de ma�ana y que tenga el mismo trabajo que el Sr. Nu�ez.
select apellido, turno, funcion, salario
from plantilla
where salario > (select max(salario) + 500000

from plantilla
where upper(turno) = 'M')
and funcion in (select funcion
from plantilla
where upper(apellido) like 'NU%');
--45. Queremos averiguar el apellido del individuo mas antiguo de la empresa.
SELECT
    apellido,
    fecha_alta AS Fecha
FROM emp
WHERE fecha_alta = (SELECT MIN(fecha_alta) FROM emp);

--46. Presentar los nombres y oficios de los empleados que tienen el mismo trabajo que 
--JIMENEZ.
SELECT
    apellido,
    oficio
FROM emp
WHERE oficio IN (SELECT oficio FROM emp WHERE UPPER(apellido) = 'JIMENEZ');

--47. Queremos conocer el apellido, oficio, salario y departamento en el que trabajan, de todos 
--los individuos cuyo salario sea mayor que el mayor salario del departamento 30.
SELECT
    apellido,
    oficio,
    salario,
    dept_no
FROM emp
WHERE salario > (SELECT MAX(salario) FROM emp WHERE dept_no = 30);

--48. Presentar los nombres y oficios de todos los empleados del departamento 20, cuyo trabajo 
--sea id�ntico al de cualquiera de los empleados del departamento de VENTAS.
select apellido, oficio
from emp
where dept_no = 20
and upper(oficio) in (select oficio

from emp e, dept2 d
where upper(dnombre)= 'VENTAS' and
e.dept_no = d.dept_no);

--49. Se desea obtener todos los empleados de los departamentos que no ganan ni el maximo ni el 
--m�nimo salarial de la empresa.SELECT
    apellido,
    oficio
FROM emp
WHERE salario <> (SELECT MAX(salario) FROM emp)
AND salario <> (SELECT MIN(salario) FROM emp);


---50. Se desea obtener el maximo salario por departamento, sin tener en cuenta a aquellos 
--empleados cuyo apellido empieza con la inicial de alguno de los empleados que tienen el 
--maximo salario de alg�n departamento. Tampoco queremos obtener los datos de departamentos 
--con menos de 3 personas muestreadas.
SELECT MAX(salario) AS maximo, dept_no
FROM emp
WHERE SUBSTRING(apellido, 1, 1) NOT IN (
    SELECT SUBSTRING(apellido, 1, 1)
    FROM emp
    WHERE (salario, dept_no) IN (
        SELECT MAX(salario), dept_no
        FROM emp
        GROUP BY dept_no
    )
)
GROUP BY dept_no
HAVING COUNT(*) > 2;



--51. Se desea averiguar el numero de oficios por departamento, sin tener en cuenta en el 
--muestreo a aquellos individuos que est�n en alguno de los departamentos que contienen 
--VENDEDORES. La salida de la consulta ser� como la siguiente.
SELECT COUNT(DISTINCT oficio) AS numero, d.dept_no AS num_dep, dnombre AS nombre
FROM emp e
RIGHT JOIN dept2 d ON e.dept_no = d.dept_no
WHERE d.dept_no NOT IN (SELECT dept_no FROM emp WHERE UPPER(oficio) LIKE 'VENDE%')
GROUP BY d.dept_no, dnombre;


--52. Sacar con el formato que aparece abajo, el apellido departamento y sueldo del empleado 
--que mas gana en la empresa y del que menos.
SELECT e1.dept_no AS dep, e1.salario AS maximo, e1.apellido AS apellido,
       e2.dept_no AS dep, e2.salario AS minimo, e2.apellido AS apellido
FROM emp e1
JOIN emp e2 ON e1.salario IN (SELECT MAX(salario) FROM emp)
            AND e2.salario IN (SELECT MIN(salario) FROM emp);


--53. En que departamento se dio de alta a mas empleados en Diciembre.
SELECT d.dnombre, COUNT(*) AS TotalEmpleados
FROM dept2 d
JOIN emp e ON e.dept_no = d.dept_no
WHERE MONTH(FECHA_ALTA) = 12
GROUP BY d.dnombre
HAVING COUNT(*) >= (
    SELECT MAX(EmpleadosPorDepartamento)
    FROM (
        SELECT COUNT(*) AS EmpleadosPorDepartamento
        FROM emp
        WHERE MONTH(FECHA_ALTA) = 12
        GROUP BY dept_no
    ) AS Subquery
);


/*
54. Se desea obtener, para cada departamento, su m�nimo y su maximo salarial. Para ello, no 
se tendr� en cuenta a los empleados cuya primera letra de su apellido, coincida con la inicial del 
nombre del departamento en que trabajan. Asimismo, se tendr� en cuenta a aquellos 
departamentos cuya diferencia entre el maximo y el m�nimo exceda la media salarial de toda la 
empresa.*/ 
SELECT e.dept_no, MIN(e.salario) AS minimo, MAX(e.salario) AS maximo
FROM emp e
WHERE SUBSTRING(e.apellido, 1, 1) NOT IN (
    SELECT SUBSTRING(d.dnombre, 1, 1)
    FROM dept2 d
    WHERE e.dept_no = d.dept_no
)
GROUP BY e.dept_no
HAVING MAX(e.salario) - MIN(e.salario) > (
    SELECT AVG(salario)
    FROM emp
);


--55. Queremos saber el nombre de el empleado mas joven de cada departamento, as� como el 
--nombre de este.select dnombre, e.dept_no numero, apellido, FECHA_ALTA
from emp e, dept2 d
where e.dept_no = d.dept_no and
FECHA_ALTA = (select max(FECHA_ALTA)
from emp e2
where e.dept_no = e2.dept_no
group by dept_no);

 --56. Se desea saber el nombre, oficio y departamento del empleado que m�s gana del 
--departamento con la media salarial m�s alta.
select apellido, oficio, dept_no
from emp e
where salario in (select max(salario)
from emp e2
where e.dept_no = e2.dept_no
group by dept_no
having avg(salario) in

(select max(avg(salario))
from emp
group by dept_no));



--57. Se desea obtener informaci�n sobre todos los empleados que son jefes de alguien.

SELECT e1.apellido, e1.oficio, e1.dept_no
FROM emp e1
WHERE EXISTS (SELECT *
              FROM emp e2
              WHERE e1.emp_no = e2.dir)
ORDER BY apellido;

----58. Recuperar el numero (empleado_no) y nombre de las personas que perciban un salario > 
--que la media de su hospital.
SELECT apellido, empleado_no
FROM plantilla p
WHERE salario > (SELECT AVG(salario)
                 FROM plantilla p2
                 WHERE p2.hospital_cod = p.hospital_cod);



--59. Insertar en la tabla Plantilla al Garcia J. con un sueldo de 3000000 ptas, y n�mero de 
--empleado 1234. Trabaja en el hospital 22, sala2.
INSERT INTO plantilla (hospital_cod, sala_cod, empleado_no, apellido, funcion, turno, salario)
VALUES (22, 2, 1234, 'Garcia J.', 'Enfermo', 'M', 3000000);

---60. Insertar la misma fila anterior sin indicar en que campos se insertan. )Por qu� no se 
--indican?insert into plantilla
values (22,2,1234,'Garcia J.','Enfermero','M',3000000)/*--61. insert into plantilla
(empleado_no, apellido)
values (1234,'Garcia J)
Esta inserci�n falla. )Por qu�?.*/
insert into plantilla
(empleado_no, apellido)
values (1234,'Garcia J')


/*62. insert into plantilla 
(hospital_cod, sala_cod, empleadono, apellido)
values (2,22,1234,'Garcia J');
En esta inserci�n no se contemplan todos los campos de la
tabla, )Falla la inserci�n?.*/INSERT INTO plantilla (hospital_cod, sala_cod, empleado_no, apellido)
VALUES (2, 22, 1234, 'Garcia J');--63. Cambiar alpaciente (tabla ENFERMO) n�mero 74835 la direcci�n a Alcala 411.update enfermo
set direccion = 'Alcala 411'
where inscripcion = 74835--64. Poner todas las direcciones de la tabla ENFERMO a nullUPDATE enfermo
SET direccion = NULL;
--65. Igualar la direcci�n y fecha de nacimiento del paciente 10995 a los valores de las 
--columnas correspondientes almacenadas para el paciente 14024.UPDATE enfermo
SET direccion = e.direccion, fecha_nac = e.fecha_nac
FROM enfermo AS e
WHERE ENFERMO,_INSCRIPCION = 10995
AND e.inscripcion = 14024;
--66. En todos los hospitales del pa�s se ha recibido un aumento del presupuesto, por lo que se 
--incrementar� el n�mero de camas disponibles en un 10%. )Como se har�a en SQL?.
SELECT * INTO hospitales2 FROM hospital;

ALTER TABLE hospitales2
ALTER COLUMN num_cama INT;

UPDATE hospitales2
SET num_cama = num_cama + (num_cama * 0.1);
--67.-Ejercicio a comentar su soluci�n.CREATE TABLE hospitales22 (
    hospital_cod SMALLINT,
    nombre NVARCHAR(15),
    direccion NVARCHAR(20),
    telefono CHAR(8),
    num_cama SMALLINT,
    CONSTRAINT PK_hospital_cod PRIMARY KEY (hospital_cod)
);

--68.- Rellenar la tabla HOSPITALES22 con las filas de HOSPITAL. Da esto alg�n problema? 
--Por que?
INSERT into hospitales22
select * from hospital

--69. Crearse una tabla llamada VARONES con la misma estructura que la tabla enfermo.
drop table varones
create table varones
(inscripcion int not null,
apellido varchar(25),
direccion varchar(12),
fecha_nac date,
s char(1),
nss int);

--70. Crear la tabla EMPLEADOS con la misma estructura que la tabla Emp y conteniendo los 
--datos de oficio PRESIDENTE o comisi�n mayor que el 25% del salario.create table empleados
as
select * from emp
where upper(oficio) = 'DIRECTOR' or
COMISION > 0.25*salario-------------------------------CREACI�N DE VISTAS
--71. - Crear una vista para los departamentos 10 y 20.
--- Crear una vista para los departamentos 10 y 30.
--- Hacer una JOIN de las dos vistas anteriores.-- Vista para los departamentos 10 y 20
-- Crear la vista emp10
-- Crear la vista emp10
CREATE VIEW emp10 AS
SELECT *
FROM emp
WHERE dept_no IN (10, 20);

CREATE VIEW emp30 AS
SELECT *
FROM emp
WHERE dept_no IN (10, 30);

SELECT e1.dept_no, e2.dept_no, e1.apellido, e2.oficio, 
       e1.salario, ISNULL(e1.comision, 0)
FROM emp10 e1
INNER JOIN emp30 e2 ON e1.dept_no = e2.dept_no;


---72. Hacer una JOIN de la tabla DEPT2 y la vista de los departamentos 10 y 20.select e1.dept_no, e2.dept_no, e1.apellido, e1.oficio,
e1.salario, nvl(e1.comision,0)
from emp10 e1, emp e2
where e1.dept_no = e2.dept_no--73. Se va a realizar un programa de consulta de la informaci�n sobre enfermos. Los datos a 
--mostrar ser�n sus apellidos, direcci�n, fecha de nacimiento y hospital en el que se encuentran. 
--)Qu� vista se definir�?. )Es posible modificar datos a trav�s de la vista anterior?.CREATE VIEW enferm
AS
SELECT e.apellido, e.direccion, e.fecha_nac, h.nombre
FROM enfermo e
INNER JOIN ocupacion o ON e.inscripcion = o.inscripcion
INNER JOIN hospital h ON o.hospital_cod = h.hospital_cod;--74. create view emp_cua
--as select dept_no, sum(salario) salariototal 
-- from emp
-- Estudiar esta vista.create view emp_cua
as select dept-no,sum(salario) salariototal
from emp
group by dept_no--75. Crear una vista para el departamento 10 con la cl�usula with check option. )Qu� ocurre?.create view emp_ter
where dept_no = 10
with check option