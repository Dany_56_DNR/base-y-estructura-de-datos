USE SQL


---TABLA PROEVEEDORES 
CREATE TABLE Proveedores
(P# CHAR(3),
PNombre VARCHAR(20),
Categoria INT,
Ciudad VARCHAR(30),
CONSTRAINT P#_pk PRIMARY KEY(P#))

--TABLA COMPONENTES

CREATE TABLE Componentes
(C# CHAR(3),
CNombre VARCHAR(20),
Color CHAR(10),
Peso INT,
Ciudad VARCHAR(30),
CONSTRAINT C#_pk PRIMARY KEY(C#))


---TABLA ARTICULOS

CREATE TABLE Articulos
(T# CHAR(3),
TNombre VARCHAR(20),
Ciudad VARCHAR(30),
CONSTRAINT T#_pk PRIMARY KEY(T#))--TABLA ENVIOSCREATE TABLE Envios
(P# CHAR(3) CONSTRAINT P# REFERENCES Proveedores(P#),
C# CHAR(3) CONSTRAINT C# REFERENCES Componentes(C#),
T# CHAR(3) CONSTRAINT T# REFERENCES Articulos(T#),
Cantidad INT)


--INSERTAMOS LOS DATOS DE LA TABLA COMPONENTES

insert into componentes
values ('c1','x3a','rojo',12,'sevilla')

--INSERTAR VARIOS REGISTROS A LA VEZ

insert into componentes
values ('c2','b85','verde',17,'madrid'),
('c3','c4b','azul',17,'malaga'),
('c4','c4b','rojo',14,'sevilla'),
('c5','vt8','azul',12,'madrid'),
('c6','c30','rojo',19,'sevilla')


 
---INSERTAMOS LOS DATOS DE LA TABLA PROVEEDORES

insert into proveedores
values ('p1','carlos',20,'sevilla'),
('p2','juan',10,'madrid'),
('p3','jose',30,'sevilla'),
('p4','inma',20,'sevilla'),
('p5','eva',30,'caceres')--INSERTAMOS LOS DATOS DE LA TABLA ARTICULOSinsert into articulos
values ('t1','clasificadora','madrid'),
('t2','perforadora','malaga'),
('t3','lectora','caceres'),
('t4','consola','caceres'),
('t5','mezcladora','sevilla'),
('t6','terminal','barcelona'),
('t7','cinta','sevilla')--�Qu� pasa cuando no se incluyen todos los valores para las columnas?insert into articulos
values ('t1','clasificadora')

insert into articulos (t#,tnombre)
values ('t1','clasificadora')

--INSERTAMOS LOS DATOS DE LA TABLA ENVIOS

insert into envios
values ('p1','c1','t1',200),
('p1','c1','t4',700),
('p2','c3','t1',400),
('p2','c3','t2',200),
('p2','c3','t3',200),
('p2','c3','t4',500),
('p2','c3','t5',600),
('p2','c3','t6',400),
('p2','c3','t7',800),
('p2','c5','t2',100),
('p3','c3','t1',200),
('p3','c4','t2',500),
('p4','c6','t3',300),
('p4','c6','t7',300),
('p5','c2','t2',200),
('p5','c2','t4',100),
('p5','c5','t4',500),
('p5','c5','t7',100),
('p5','c6','t2',200),
('p5','c1','t4',100),
('p5','c3','t4',200),
('p5','c4','t4',800),
('p5','c5','t5',400),
('p5','c6','t4',500)



---1. Obtener todos los detalles de todos los art�culos de CACERES.
SELECT *
FROM Articulos
WHERE ciudad='caceres'

---2. Obtener todos los valores de P# para los proveedores que abastecen el articulo Tl

SELECT P#
FROM ENVIOS
WHERE T#= 'T1'


---3. Obtener la lista de pares de atributos {COLOR, CIUDAD) de la tabla componentes 
--eliminando los pares duplicados.

SELECT DISTINCT COLOR, CIUDAD
FROM COMPONENTES


---4. Obtener de la tabla de art�culos los valores de T# y CIUDAD donde el nombre de la 
--ciudad acaba en D o contiene al menos una E.

SELECT T#, CIUDAD
FROM ARTICULOS
WHERE CIUDAD LIKE '%D' 
OR CIUDAD LIKE '%E%'


---5. Obtener los valores de P# para los proveedores que suministran para el art�culo T1
--el componente Cl.SELECT P#
FROM ENVIOS
WHERE T#='T1' AND C#='C1'------6. Obtener los valores de TNOMBRE en orden alfab�tico para los art�culos 
--abastecidos por el proveedor P1.

SELECT TNOMBRE
FROM ARTICULOS INNER JOIN ENVIOS 
ON ENVIOS.P#='P1' AND ENVIOS.T#=ARTICULOS.T#

-----7. Obtener los valores de C# para los componentes suministrados para cualquier 
--articulo de MADRID.
SELECT DISTINCT ENVIOS.C#
FROM ARTICULOS INNER JOIN ENVIOS 
ON ARTICULOS.T#=ENVIOS.T# 
WHERE ARTICULOS.CIUDAD='MADRID'


---8. Obtener todos los valores de C# de los componentes tales que ning�n otro 
--componente tenga un valor de peso inferior.

SELECT c#
FROM componentes
WHERE peso = (SELECT MIN(peso)
FROM componentes );

---9. Obtener los valores de P# para los proveedores que suministren los art�culos T1 y 
--T2.

SELECT ENVIOS.P#
FROM ENVIOS
WHERE T#='T1'
INTERSECT
SELECT ENVIOS.P#
FROM ENVIOS 
WHERE T#='T2'


---10. Obtener los valores de P# para los proveedores que suministran para un art�culo 
--de SEVILLA o MADRID un componente ROJO.


SELECT DISTINCT ENVIOS.P#
FROM (ENVIOS JOIN COMPONENTES
ON ENVIOS.C#=COMPONENTES.C#)
JOIN ARTICULOS
ON ENVIOS.T#=ARTICULOS.T#
WHERE (ARTICULOS.CIUDAD='SEVILLA' OR ARTICULOS.CIUDAD='MADRID') AND 
(COMPONENTES.COLOR='ROJO')

---11. Obtener mediante subconsultas los valores de C# para los componentes 
--suministrados para alg�n art�culo de SEVILLA por un proveedor de SEVILLA.

SELECT DISTINCT C#
FROM ENVIOS
WHERE T# IN( SELECT T#
FROM ARTICULOS
WHERE CIUDAD='SEVILLA')
AND P# IN ( SELECT P#
FROM PROVEEDORES
WHERE CIUDAD='SEVILLA')



---12. Obtener los valores para los art�culos que usan al menos un componente que se 
--puede obtener con el proveedor P1.

SELECT DISTINCT T#
FROM ENVIOS
WHERE C# IN( SELECT DISTINCT C#
FROM ENVIOS
WHERE P#='P1')



---13. Obtener todas las ternas (CIUDAD, C#, CIUDAD) tales que un proveedor de la 
--primera ciudad suministre el componente especificado para un art�culo montado en 
--la segunda ciudad.

SELECT P.Ciudad ,E.C#, A.Ciudad 
FROM Envios E, Proveedores P, Articulos A
WHERE E.P#=P.P# AND E.T#=A.T#; 


--14. Repetir el ejercicio anterior pero sin recuperar las ternas en los que los dos 
--valores de ciudad sean los mismos.

SELECT P.Ciudad, C#, A.Ciudad 
FROM Envios E, Proveedores P , Articulos A
WHERE E.P#=P.P# AND E.T#=A.T# AND P.Ciudad <> A.Ciudad